# Code Refactor Starter Code

The source for this project is found here https://github.com/coding-boot-camp/urban-octo-telegram
The code base is almost entirely the same, however, there are changes made to improve accessibility.

The Tasks and User Story represent what was asked on this assignment and the Acceptance Criteria represents what was reviewed and updated.

---

# Resources

    - https://www.w3schools.com/html/html5_semantic_elements.asp
    - https://www.w3schools.com/accessibility/accessibility_semantic_elements.php
    - https://stackoverflow.com/questions/3253775/css-class-and-id-with-the-same-name

---

## Tasks

**Refactoring** existing code (improving it without changing what it does) to meet a certain set of standards or to implement a new technology is a common task for front-end and junior developers. For this particular Challenge, a marketing agency has hired you to refactor an existing site to make it more accessible.

## User Story

**AS** A marketing agency
_I WANT_ a codebase that follows accessibility standards
_SO THAT_ our own site is optimized for search engines

## Acceptance Criteria

GIVEN a webpage meets accessibility standards

[x] **WHEN** I view the source code
_THEN_ I find semantic HTML elements

[x] **WHEN** I view the structure of the HTML elements
_THEN_ I find that the elements follow a logical structure independent of styling and positioning

[x] **WHEN** I view the icon and image elements
_THEN_ I find accessible alt attributes

[x] **WHEN** I view the heading attributes
_THEN_ they fall in sequential order

[x] **WHEN** I view the title element
_THEN_ I find a concise, descriptive title
